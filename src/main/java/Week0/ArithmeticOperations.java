package Week0;

import java.util.Scanner;

public class ArithmeticOperations {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a,b,c,d;
		System.out.println("First Number is : ");
		int n1 = sc.nextInt();
		System.out.println("Second Number is :");
		int n2 = sc.nextInt();
		a= n1+n2;
		b=n1-n2;
		c=n1*n2;
		d=n1/n2;		
		System.out.println("Addition of Two numbers : " +a);
		System.out.println("Multiplication of Two numbers : " +c);
		System.out.println("Subtraction of Two numbers : " +b);
		System.out.println("Division of Two numbers : " +d);
		sc.close();
	
	}

}
