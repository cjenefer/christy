package Week0;

import java.util.Scanner;

public class ConditionalOperator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("First Number is : ");
		int n1 = sc.nextInt();
		System.out.println("Second Number is :");
		int n2 = sc.nextInt();
		System.out.println("N1 is greater than or equal to N2 : "+(n1>=n2));
		System.out.println("N1 is less than or equal to N2 : "+(n1<=n2));
		System.out.println("N1 not equal to N2 : "+(n1!=n2));	
		System.out.println("N1 equal to N2 : "+(n1==n2));
		System.out.println("N1 Greater than N2 : "+(n1>n2));
		System.out.println("N1 Less than N2 : "+(n1<n2));
				sc.close();
	

	}

}
