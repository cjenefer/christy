package Week0;

import java.util.Scanner;

public class EvenOrOdd {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		int i = sc.nextInt();
		if(i%2 == 0)
		{
			System.out.println("The Given Number is Even Number");
		}
		else 
			System.out.println("The Given Number is Odd Number");
		sc.close();
	}

}
