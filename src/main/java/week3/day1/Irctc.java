package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.findElementById("userRegistrationForm:userName").sendKeys("christyjenefer");
		driver.findElementById("userRegistrationForm:password").sendKeys("8056384257@Cc");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("8056384257@Cc");
		WebElement secques = driver.findElementById("userRegistrationForm:securityQ");
		Select sq = new Select(secques);
		sq.selectByIndex(2);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("vijay");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Christy Jenefer");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("P");
		driver.findElementById("userRegistrationForm:gender:1").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select occ = new Select(occupation);
		occ.selectByVisibleText("Private");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select coun = new Select(country);
		coun.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("cjchristy337@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("8056384257");
		WebElement nation = driver.findElementById("userRegistrationForm:nationalityId");
		Select nat = new Select(nation);
		nat.selectByValue("94");;
		driver.findElementById("userRegistrationForm:address").sendKeys("No.80,Arcot Road");
		driver.findElementById("userRegistrationForm:area").sendKeys("Valasarvakkam");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600087",Keys.TAB);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select ci = new Select(city);
		ci.selectByVisibleText("Tiruvallur");
	/*	List<WebElement> options = ci.getOptions();
		int size=options.size();
		ci.selectByIndex(size-1);*/
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select po = new Select(post);
		po.selectByValue("Valasaravakkam S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("8610193554");
//driver.close();
	}

}
