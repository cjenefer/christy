package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class CreateLead {
@Test
	public void create() {
			
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
				ChromeDriver driver = new ChromeDriver();
				driver.manage().window().maximize();
		
				driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
				driver.get("http://leaftaps.com/opentaps");
			/*	try
				{
				driver.findElementById("username1").sendKeys("DemoSalesManager");
				}
				catch (Exception e) {
					System.out.println("Exception Thrown");
					throw new RuntimeException();
					}
				finally
				{
					driver.close();
				}*/
				driver.findElementById("username").sendKeys("DemoSalesManager");
				driver.findElementById("password").sendKeys("crmsfa");
				driver.findElementByClassName("decorativeSubmit").click();
				driver.findElementByPartialLinkText("CRM/SFA").click();
				driver.findElementByPartialLinkText("Create Lead").click();
				driver.findElementById("createLeadForm_companyName").sendKeys("IBM INdia Pvt LTD");
				driver.findElementById("createLeadForm_firstName").sendKeys("DivChris");
				driver.findElementById("createLeadForm_lastName").sendKeys("SJ");
				WebElement src = driver.findElementById("createLeadForm_dataSourceId");
				Select dropdown = new Select(src);
				dropdown.selectByVisibleText("Cold Call");
				WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
				Select dropdown2 = new Select(src2);
List<WebElement> options = dropdown2.getOptions();
int size = options.size();
dropdown2.selectByIndex(size-2);
	
			/*finally
			{
				driver.close();
			}*/

		driver.findElementByClassName("smallSubmit").click();
		driver.close();
	}
}
