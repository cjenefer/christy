package week3.day1;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LeafGround {

	public static void main(String[] args) {

System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
ChromeDriver driver = new ChromeDriver();
driver.manage().window().maximize();
driver.get("http://leafground.com/pages/table.html");
List<WebElement> checkBox = driver.findElementsByXPath("//input[@type='checkbox']");
int size = checkBox.size();
System.out.println(size);
WebElement check = checkBox.get(size-1);
check.click();
	}

}
