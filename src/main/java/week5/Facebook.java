package week5;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

public class Facebook {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String searchfield = "TestLeaf";
		String button = "Like";
		String button2 = "Liked";
		// Actions builder = new Actions(driver);
		driver.get("https://www.facebook.com/");
		driver.findElementByXPath("//input[@id = \"email\"]").sendKeys("9003516463");
		driver.findElementById("pass").sendKeys("divyamet12");
		driver.findElementByXPath("//input[@type =\"submit\"]").click();
		driver.findElementByXPath("//input[@class =\"_1frb\"]").sendKeys(searchfield);
		driver.getKeyboard().sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		WebElement searchclick = driver.findElementByXPath("//div[text() = '" + searchfield + "']");
		String text = searchclick.getText();
		if (text.equals(searchfield)) {
			System.out.println("TestLeaf is present in the result");
			WebElement likeclick = driver.findElementByXPath("(//button[@type = \"submit\"])[2]");
			String text2 = likeclick.getText().trim();
			System.out.println(text2);
			if (text2.equals(button)) {
				System.out.println("Inside LIKE ");
				likeclick.click();
			} 
			else if (text2.equals(button2)) {
				System.out.println(" Inside LIKED");
				searchclick.click();
				String title = driver.getTitle();
				String text3 = driver.findElementByXPath("(//div[@class=\"_4bl9\"])[3]").getText();
				System.out.println(text3);
				System.out.println(text3.replaceAll("\\D", ""));
				if(title.contains(searchfield))
				{
			System.out.println("Title contains TestLeaf");
				}

			} 
		}
		else {
			System.out.println("TestLeaf is not present in the result");
		}
		driver.close();
	}
}

