package week5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBugs {
	@Test
	public void fix() throws InterruptedException {
	//public void main(String args) throws InterruptedException {

		// launch the browser
		System.setProperty("webdriver.chrome.driver","./driver/chromedriver.exe");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.myntra.com/");
		
		// Mouse Over on Men
		Actions builder = new Actions(driver);
		builder.moveToElement(driver.findElementByLinkText("Men")).perform();

		// Click on Jackets
		driver.findElementByXPath("//a[@href='/men-jackets']").click();


		// Find the count of Jackets
		String leftCount = 
				driver.findElementByXPath("//input[@value='Jackets']/following-sibling::span").getText().replaceAll("[^\\d]", "");
		System.out.println(leftCount);

		// Click on Jackets and confirm the count is same
		driver.findElementByXPath("//label[text()='Jackets']").click();
		
		// Wait for some time
		Thread.sleep(5000);

		// Check the count
		String rightCount = 
				driver.findElementByXPath("//h1[text()='Mens Jackets']/following-sibling::span").getText().replaceAll("[^\\d]", "");
		System.out.println(rightCount);
		
		// If both count matches, say success
		if(rightCount.equals(leftCount)) {
			System.out.println("The count matches on either case");
		}else {
			System.out.println("The count does not match");
		}

		// Click on Offers
		//driver.findElementByXPath("//h3[text()='Offers']").click();

		// Find the costliest Jacket
		List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
		List<String> onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("[^\\d]", ""));
		}

		// Sort them 
		String max = Collections.max(onlyPrice);

		// Find the top one
		System.out.println(max);
		
		// Print Only Allen Solly Brand Minimum Price
		driver.findElementByClassName("brand-more").click();
		driver.findElementByXPath("//input[@value='Allen Solly']").click();
		driver.findElementByXPath("myntraweb-sprite FilterDirectory-close sprites-remove").click();
		// Get the minimum Price 
				String min = Collections.min(onlyPrice);

				// Find the lowest priced Allen Solly
				System.out.println(min);
				
		/*// Find the costliest Jacket
		List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

		onlyPrice = new ArrayList<String>();

		for (WebElement productPrice : productsPrice) {
			onlyPrice.add(productPrice.getText().replaceAll("//D", ""));
		}

		

		driver.close();*/

	}
	}