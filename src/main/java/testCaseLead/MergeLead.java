package testCaseLead;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC_MergeLead";
		testCaseDesc = "Merge a Lead";
		category = "smoke";
		author = "Christy";
	}
@Test
	public void merge(){
	String lead = "10601";
	driver.findElementByXPath("//a[text() = 'Leads']").click();
	driver.findElementByXPath("//a[text() = 'Merge Leads']").click();
	driver.findElementByXPath("//img[@alt = \"Lookup\"][1]").click();
Set<String> windows = driver.getWindowHandles();
List<String> listofwindows = new ArrayList<>();
listofwindows.addAll(windows);
driver.switchTo().window(listofwindows.get(1));
driver.manage().window().maximize();
driver.findElementByXPath("//input[@autocomplete = \"off\"][1]").sendKeys(lead);
driver.findElementByXPath("//button[text()='Find Leads']").click();
driver.findElementByXPath("//a[text() = '"+lead+"']").click();	
driver.switchTo().window(listofwindows.get(0));
driver.findElementByClassName("buttonDangerous").click();
driver.switchTo().alert().accept();
String text = driver.findElementByXPath("//div[@class = \"messages\"]").getText();
System.out.println(text);
driver.close();
	}

}
