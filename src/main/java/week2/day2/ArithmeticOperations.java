package week2.day2;

import java.util.Scanner;

public class ArithmeticOperations {

	public static void main(String[] args) {
		int i,j,a;
		Scanner sc =new Scanner(System.in);
		System.out.println("Enter the Two Numbers : ");
		i=sc.nextInt();
		j=sc.nextInt();
		System.out.println("Enter the Choices (Add/Subtract/Multiply/Divide) : ");
		String str = sc.next();
		switch(str)
		{
		case "Add":
			a =i+j;
			System.out.println("Addition of two Numbers : "+a);
			break;
		case "Subtract":
			a=i-j;
			System.out.println("Subtraction of two Numbers : "+a);
			break;
		case "Multiply":
			a=i*j;
			System.out.println("Multiplication of two Numbers : "+a);
			break;
		case "Divide":
			a=i/j;
			System.out.println("Division of two Numbers : "+a);
			break;
		default:
			System.out.println("Out of Choice");

		}
	}

}
