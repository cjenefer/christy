package week2.day2;

import java.util.Scanner;

public class PrimeNumbers30 {

	public static void main(String[] args) {
		int i,a,b,count=0;
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the value of n :");
		i=sc.nextInt();
		for(a=1;a<=i;a++)
		{
			count =0;
			for(b=1;b<=a;b++)
			{
				if(a%b == 0)
				{
					count++;
				}
			}
			if(count ==2)
			{
				System.out.print(a+ " ");
			}
		}

	}

}
