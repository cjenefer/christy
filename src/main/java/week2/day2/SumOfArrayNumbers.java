package week2.day2;

import java.util.Scanner;

public class SumOfArrayNumbers {

	public static void main(String[] args) {
		int sum =0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array : ");
		int size = sc.nextInt();
		System.out.println("Enter the Values :" );
		int array[]= new int[size];
		for (int i =0;i<size;i++)
		{
			array[i]=sc.nextInt();
			sum = sum+array[i];
		}
		System.out.println("Sum of Numbers in array : "+sum);


	}

}
