package week2.day2;

import java.util.Scanner;

public class FizzBuzz {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int a=sc.nextInt();
		int b=sc.nextInt();
		for(int i=a;i<=b;i++)
		{
			if(i%3 ==0 && i%5 ==0 )
			{
				System.out.print("FIZZBUZZ ");
			}
			else if(i%3 == 0)
			{
				System.out.print("FIZZ ");
			}
			else if(i%5==0)
			{
				System.out.print("BUZZ ");
			}
			else
				System.out.print(i+ " ");
		}
	}

}
