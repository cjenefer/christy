package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnSet {

	public static void main(String[] args) {
		Set<String> mobiles = new LinkedHashSet<String>();
		mobiles.add("Nokia");
		mobiles.add("Samsung");
		mobiles.add("Moto");
		boolean b = mobiles.add("Samsung");
		mobiles.add("Iphone");
		for (String c : mobiles) {
			System.out.println(c);
			
		}
		System.out.println(b);
		List<String> str = new ArrayList<String>();
		str.addAll(mobiles);
		String s = str.get(0);
		System.out.println(s);
		
	}

}
