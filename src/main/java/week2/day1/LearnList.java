package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LearnList {

	public static void main(String[] args) {
		List<String> mobilePhones = new ArrayList<String>();
		mobilePhones.add("motorola");
		mobilePhones.add("Samsung");
		mobilePhones.add("Samsung2");
		mobilePhones.add("Vivo");
		mobilePhones.add("Iphone");
		for(String eachPhone : mobilePhones)
		{
			System.out.println(eachPhone);
		}
		int size = mobilePhones.size();
		System.out.println("The Count of Mobile Phones are : "+size);
		System.out.println("The Last Mobile Name is : "+mobilePhones.get(size-2));
		Collections.sort(mobilePhones);
		for(String eachPhone : mobilePhones)
		{
			System.out.println(eachPhone);
		}

	}

}
