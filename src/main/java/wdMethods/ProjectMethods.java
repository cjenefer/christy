package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.Excel;

public class ProjectMethods extends SeMethods{
	public String dataSheetName;
	//@Test(groups = {"smoke"})
	@BeforeSuite
	public void beforeSuite() {
		beginResult();
	}
	//@Test(groups = {"smoke"})
	@BeforeClass
	public void beforeClass() {
		startTestCase();
	}
	//@Test(groups = {"smoke"})
	
	@BeforeMethod
	@Parameters({"url"})
	public void login(String url) {
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleCRM = locateElement("linkText","CRM/SFA");
		click(eleCRM);*/
	}
	//@Test(groups = {"smoke"})
	@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	//@Test(groups = {"smoke"})
	@AfterSuite
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name = "positive")
	public Object[][] fetchData() throws IOException
	{
	return Excel.getExcelSheet(dataSheetName);
		}
	
	
	
	
	
	
}
