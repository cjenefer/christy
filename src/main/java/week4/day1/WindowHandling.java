package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandling {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text() = 'AGENT LOGIN'][1]").click();
		driver.findElementByXPath("//a[text() = 'Contact Us'][1]").click();
		Set<String> totalwindows = driver.getWindowHandles();
		List<String> listofwindows = new ArrayList<>();
		listofwindows.addAll(totalwindows);
		driver.switchTo().window(listofwindows.get(1));
		driver.manage().window().maximize();
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./snap/img.png");
		FileUtils.copyFile(src, dest);
		driver.switchTo().window(listofwindows.get(0));
		driver.close();
		
		
	}

}
