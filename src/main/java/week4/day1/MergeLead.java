package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) {
		String lead = "10601";
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://leaftaps.com/opentaps");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByPartialLinkText("CRM/SFA").click();
		driver.findElementByXPath("//a[text() = 'Leads']").click();
		driver.findElementByXPath("//a[text() = 'Merge Leads']").click();
		driver.findElementByXPath("//img[@alt = \"Lookup\"][1]").click();
Set<String> windows = driver.getWindowHandles();
List<String> listofwindows = new ArrayList<>();
listofwindows.addAll(windows);
driver.switchTo().window(listofwindows.get(1));
driver.manage().window().maximize();
driver.findElementByXPath("//input[@autocomplete = \"off\"][1]").sendKeys(lead);
driver.findElementByXPath("//button[text()='Find Leads']").click();
driver.findElementByXPath("//a[text() = '"+lead+"']").click();	
driver.switchTo().window(listofwindows.get(0));
driver.findElementByClassName("buttonDangerous").click();
driver.switchTo().alert().accept();
String text = driver.findElementByXPath("//div[@class = \"messages\"]").getText();
System.out.println(text);
driver.close();
	}

}
