package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel {

	public static Object[][] getExcelSheet(String dataSheetName) throws IOException {
		XSSFWorkbook wbook = new XSSFWorkbook("./Data/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wbook.getSheet("Sheet1");
		int columncount = sheet.getRow(0).getLastCellNum();
		int rowcount = sheet.getLastRowNum();
		Object[][] data = new Object[rowcount][columncount];
		for (int i = 1; i <= rowcount; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < columncount; j++) {
				XSSFCell cell = row.getCell(j);
				data[i-1][j] = cell.getStringCellValue();
				//System.out.println(da);
			} 
		}
		return data;

	}

}
