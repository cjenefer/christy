package testcase;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Pages.LoginPage;
import wdMethods.ProjectMethods;

public class Tc002 extends ProjectMethods{
	@BeforeTest
public void setData()
{
	testCaseName = "TC_CreateLead";
	testCaseDesc = "Create a New Lead";
	category = "smoke";
	author = "Christy";
	dataSheetName = "CreateLead";

}
@Test (dataProvider = "positive")
	public void createLead(String username,String pwd,String fname,String lname,String cname)
	{
		new LoginPage().typeUserName(username).typePassword(pwd).clickLogin().ClickCRM().ClickLead().ClickCreateLead().FirstName(fname)
		.LastName(lname).CompanyName(cname).CreateLeadButton().VerifyFirstName(fname);
	}
}
