package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods{

	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id = "username")
	WebElement eleUserName;
	public LoginPage typeUserName(String usname)
	{
	type(eleUserName, usname);
	return this;
	}
	@FindBy(id = "password")
	WebElement elePassword;
	public LoginPage typePassword(String pwd)
	{
		type(elePassword,pwd);
		return this;
	}
	@FindBy(how =How.CLASS_NAME,using = "decorativeSubmit")
	WebElement clickloginbutton;
	public HomePage clickLogin()
	{
		click(clickloginbutton);
		return new HomePage();
	}
}

