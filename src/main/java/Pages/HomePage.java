package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	public void VerifyLoggedUserName(String ExpectedResult)
	{
		WebElement verifyloggeduser = locateElement("xpath","//h2[text()='Demo Sales Manager']");
		verifyPartialText(verifyloggeduser,ExpectedResult);
	}
	public MyHome ClickCRM()
	{
	WebElement clicklink = locateElement("linkText","CRM/SFA");
	click(clicklink);
	return new MyHome();
		
	}

}
