package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods {
	public MyLeads()
	{
		PageFactory.initElements(driver, this);
	}
	public CreateLead ClickCreateLead()
	{
		WebElement clickcreate = locateElement("xpath","//a[text()='Create Lead']");
		click(clickcreate);
		return new CreateLead();
	}

}
