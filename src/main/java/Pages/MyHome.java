package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{
	public MyHome()
	{
		PageFactory.initElements(driver, this);
	}

		public MyLeads ClickLead()
		{
			WebElement clickLeadsLink = locateElement("xpath","//a[text()='Leads']");
			click(clickLeadsLink);
			return new MyLeads();
		}
	

}
