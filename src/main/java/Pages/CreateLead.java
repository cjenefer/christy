package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	public CreateLead FirstName(String fname)
	{
		WebElement enterfirstName = locateElement("id","createLeadForm_firstName");
		type(enterfirstName,fname);
		return this;
	}
	public CreateLead LastName(String lname)
	{
		WebElement enterLastName = locateElement("id","createLeadForm_lastName");
		type(enterLastName,lname);
		return this;
	}
	public CreateLead CompanyName(String cname)
	{
		WebElement enterCompanyName = locateElement("id","createLeadForm_companyName");
		type(enterCompanyName,cname);
		return this;
	}
	public ViewLead CreateLeadButton()
	{
		WebElement createbuttonclick = locateElement("class","smallSubmit");
		click(createbuttonclick);
		return new ViewLead();
	}

}
