package week1.day1;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		int i,j,b,count=0;
		Scanner sc=new Scanner(System.in);
		i = sc.nextInt();
		j=sc.nextInt();
		for(int a=i;a<=j;a++)
		{
			count=0;
			for(b=1;b<=a;b++)
			{
				if(a%b == 0)
				{
					count++;
				}
			}
			if(count==2)
			{
				System.out.println(a);
			}
		}
		sc.close();
	}

}