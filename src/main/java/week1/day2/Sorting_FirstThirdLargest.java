package week1.day2;

import java.util.Scanner;

public class Sorting_FirstThirdLargest {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Size of an Array :");
		int size =sc.nextInt();
		System.out.println("The Numbers are : ");
		int n[]=new int[size];
		int a;
		for(int i=0;i<size;i++)
		{
			n[i]=sc.nextInt();
		}
		for(int i=0;i<size;i++)
		{
			for(int j=0;j<=i;j++)
			{
				if(n[i] > n[j])
				{
					a=n[i];
					n[i]=n[j];
					n[j]=a;
				}
			}
		}
		System.out.println("First Largest Number is :" + n[0]);
		System.out.println("Third Largest Number is :" + n[2]);
	}

}
