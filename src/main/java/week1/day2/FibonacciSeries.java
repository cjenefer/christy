package week1.day2;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {
		int i,t1 = 0,t2 =1,sum;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number :  ");
		int n = sc.nextInt();
		System.out.println("Fibonacci Series of "+n+" is :");
		while(t1<=n)	
		{
			System.out.print(t1+ " ");
			sum = t1+t2;
			t1=t2;
			t2=sum;
		}

		sc.close();
	}

}
