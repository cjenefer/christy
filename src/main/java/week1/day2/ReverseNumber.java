package week1.day2;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		int n,last,reverse =0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Number : ");
		n=sc.nextInt();
		while(n!=0)
		{
		last = n%10;
		reverse = reverse *10+last;
		n = n/10;;
		}
		System.out.println("Reverse of Given Number is :");
		System.out.println(reverse);
sc.close();
	}

}
